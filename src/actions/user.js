import { registerUser, loginUser, editUser, fetchAllUsers, delUser } from '../utils';

export const REGISTER_USER = 'REGISTER_USER';
export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAIL = 'REGISTER_USER_FAIL';
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const USER_UPDATE_FAIL = 'USER_UPDATE_FAIL';
export const USER_UPDATE_SUCCESS = 'USER_UPDATE_SUCCESS';
export const UPDATE_USER = 'UPDATE_USER';
export const TRY_UPDATE = 'TRY_UPDATE';
export const FETCH_ALL_USERS = 'FETCH_ALL_USERS';
export const GET_ALL_USERS = 'GET_ALL_USERS';
export const ERROR_GET_USERS = 'ERROR_GET_USERS';
export const DELETE_USER_SUCCESS = 'DELETE_USER_SUCCESS';
export const DELETE_USER_FAIL = 'DELETE_USER_FAIL';
export const LOGOUT_USER = 'LOGOUT_USER';

export const register = user => async (dispatch) => {
  dispatch({ type: REGISTER_USER });

  try {
    const newUser = await registerUser(user);
    dispatch({
      type: REGISTER_USER_SUCCESS,
      payload: newUser,
    });
  } catch (e) {
    dispatch({
      type: REGISTER_USER_FAIL,
      payload: e,
    });
  }
};

export const login = user => async (dispatch) => {
  dispatch({ type: REGISTER_USER });

  try {
    const info = await loginUser(user);
    dispatch({
      type: LOGIN_USER_SUCCESS,
      payload: info,
    });
  } catch (e) {
    dispatch({
      type: REGISTER_USER_FAIL,
      payload: e,
    });
  }
};

export const change = user => async (dispatch, getState) => {
  const { token } = getState().user;
  dispatch({ type: TRY_UPDATE });

  try {
    const info = await editUser(user, token);
    if (!user.id) {
      dispatch({
        type: USER_UPDATE_SUCCESS,
      });
      return dispatch({
        type: LOGIN_USER_SUCCESS,
        payload: {
          name: info.name,
          email: info.email,
          isAdmin: info.isAdmin,
          token,
        },
      });
    }
    if (user.id) {
      dispatch({
        type: USER_UPDATE_SUCCESS,
      });
      return dispatch({
        type: UPDATE_USER,
        payload: {
          id: info._id,
          name: info.name,
          email: info.email,
          isAdmin: info.isAdmin,
        },
      });
    }
    return null;
  } catch (e) {
    dispatch({
      type: USER_UPDATE_FAIL,
      payload: e,
    });
    return Promise.reject(e);
  }
};

export const getAllUsers = () => async (dispatch, getState) => {
  const { token } = getState().user;
  dispatch({ type: FETCH_ALL_USERS });

  try {
    const usersList = await fetchAllUsers(token);
    dispatch({
      type: GET_ALL_USERS,
      payload: usersList,
    });
  } catch (e) {
    dispatch({
      type: ERROR_GET_USERS,
      payload: e,
    });
  }
};

export const deleteUser = id => async (dispatch, getState) => {
  const { token } = getState().user;

  try {
    await delUser(id, token);
    dispatch({
      type: DELETE_USER_SUCCESS,
    });
  } catch (e) {
    dispatch({
      type: DELETE_USER_FAIL,
      payload: e,
    });
  }
};

export const logout = () => (dispatch) => {
  dispatch({
    type: LOGOUT_USER,
  });
};
