import { postRequest, fetchRequest, delRequest, editRequest, fetchAllRequests, fetchUserAllRequests } from '../utils';

export const POST_REQUEST = 'POST_REQUEST';
export const POST_REQUEST_SUCCESS = 'POST_REQUEST_SUCCESS';
export const POST_REQUEST_FAIL = 'POST_REQUEST_FAIL';
export const GET_REQUESTS = 'GET_REQUESTS';
export const GET_REQUESTS_SUCCESS = 'GET_REQUESTS_SUCCESS';
export const GET_REQUESTS_FAIL = 'GET_REQUESTS_FAIL';
export const EDIT_REQUEST_FAIL = 'EDIT_REQUEST_FAIL';
export const EDIT_REQUEST_SUCCESS = 'EDIT_REQUEST_SUCCESS';
export const DELETE_REQUEST_SUCCESS = 'DELETE_REQUEST_SUCCESS';
export const GET_ALL_REQUESTS_SUCCESS = 'GET_ALL_REQUESTS_SUCCESS';
export const GET_ALL_USER_REQUESTS_SUCCESS = 'GET_ALL_USER_REQUESTS_SUCCESS';

export const create = request => async (dispatch, getState) => {
  const { token } = getState().user;
  dispatch({ type: POST_REQUEST });

  try {
    const newRequest = await postRequest(token, request);
    return dispatch({
      type: POST_REQUEST_SUCCESS,
      payload: newRequest,
    });
  } catch (e) {
    dispatch({
      type: POST_REQUEST_FAIL,
      payload: e,
    });
    return Promise.reject(e);
  }
};

export const getRequests = () => async (dispatch, getState) => {
  const { token } = getState().user;
  dispatch({ type: GET_REQUESTS });

  try {
    const newRequest = await fetchRequest(token);
    dispatch({
      type: GET_REQUESTS_SUCCESS,
      payload: newRequest,
    });
  } catch (e) {
    dispatch({
      type: GET_REQUESTS_FAIL,
      payload: e,
    });
  }
};

export const deleteRequest = id => async (dispatch, getState) => {
  const { token } = getState().user;

  try {
    await delRequest(id, token);
    dispatch({
      type: DELETE_REQUEST_SUCCESS,
      payload: id,
    });
  } catch (e) {
    dispatch({
      type: EDIT_REQUEST_FAIL,
      payload: e,
    });
  }
};

export const updateRequest = (data, id) => async (dispatch, getState) => {
  const { token } = getState().user;

  try {
    const editedRequest = await editRequest(data, id, token);
    dispatch({
      type: EDIT_REQUEST_SUCCESS,
      payload: editedRequest,
    });
  } catch (e) {
    dispatch({
      type: EDIT_REQUEST_FAIL,
      payload: e,
    });
  }
};

export const getAllRequests = () => async (dispatch, getState) => {
  const { token } = getState().user;

  try {
    const requestsList = await fetchAllRequests(token);
    dispatch({
      type: GET_ALL_REQUESTS_SUCCESS,
      payload: requestsList,
    });
  } catch (e) {
    dispatch({
      type: GET_REQUESTS_FAIL,
      payload: e,
    });
  }
};

export const getAllUserRequests = id => async (dispatch, getState) => {
  const { token } = getState().user;

  try {
    const requestsList = await fetchUserAllRequests(id, token);
    dispatch({
      type: GET_ALL_USER_REQUESTS_SUCCESS,
      payload: requestsList,
    });
  } catch (e) {
    dispatch({
      type: GET_REQUESTS_FAIL,
      payload: e,
    });
  }
};
