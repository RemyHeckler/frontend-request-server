import { BASE_URL } from '../config';

const getHeaders = token => ({
  'Content-Type': 'application/json',
  Authorization: `Bearer ${token}`,
});

const getContentType = () => ({
  'Content-Type': 'application/json',
});

export const postRequest = async (token, data) => {
  const response = await fetch(`${BASE_URL}/requests`, {
    method: 'post',
    headers: getHeaders(token),
    body: JSON.stringify(data),
  });

  if (response.ok) {
    return response.json();
  }
  return Promise.reject(new Error(response.statusText));
};

export const fetchRequest = async (token) => {
  const response = await fetch(`${BASE_URL}/requests`, { headers: getHeaders(token) });

  if (response.ok) {
    return response.json();
  }
  return Promise.reject(new Error(response.statusText));
};

export const delRequest = async (id, token) => {
  const response = await fetch(`${BASE_URL}/requests?id=${id}`, {
    method: 'delete',
    headers: getHeaders(token),
  });

  if (response.ok) {
    return response;
  }
  return Promise.reject(new Error(response.statusText));
};

export const editRequest = async (data, id, token) => {
  const response = await fetch(`${BASE_URL}/requests?id=${id}`, {
    method: 'put',
    headers: getHeaders(token),
    body: JSON.stringify(data),
  });

  if (response.ok) {
    return response.json();
  }
  return Promise.reject(new Error(response.statusText));
};

export const fetchAllRequests = async (token) => {
  const response = await fetch(`${BASE_URL}/requests/list`, { headers: getHeaders(token) });

  if (response.ok) {
    return response.json();
  }
  return Promise.reject(new Error(response.statusText));
};

export const fetchUserAllRequests = async (id, token) => {
  const response = await fetch(`${BASE_URL}/requests?id=${id}`, { headers: getHeaders(token) });

  if (response.ok) {
    return response.json();
  }
  return Promise.reject(new Error(response.statusText));
};

export const registerUser = async (user) => {
  const response = await fetch(`${BASE_URL}/register`, {
    method: 'post',
    headers: getContentType(),
    body: JSON.stringify(user),
  });

  if (response.ok) {
    return response.json();
  }
  return Promise.reject(new Error(response.statusText));
};

export const loginUser = async (user) => {
  const response = await fetch(`${BASE_URL}/login`, {
    method: 'post',
    headers: getContentType(),
    body: JSON.stringify(user),
  });

  if (response.ok) {
    return response.json();
  }
  return Promise.reject(new Error(response.statusText));
};

export const editUser = async (user, token) => {
  const response = await fetch(`${BASE_URL}/user`, {
    method: 'put',
    headers: getHeaders(token),
    body: JSON.stringify(user),
  });

  if (response.ok) {
    return response.json();
  }
  return Promise.reject(new Error(response.statusText));
};

export const fetchAllUsers = async (token) => {
  const response = await fetch(`${BASE_URL}/user/list`, { headers: getHeaders(token) });

  if (response.ok) {
    return response.json();
  }
  return Promise.reject(new Error(response.statusText));
};

export const delUser = async (id, token) => {
  const response = await fetch(`${BASE_URL}/user?id=${id}`, {
    method: 'delete',
    headers: getHeaders(token),
  });

  if (response.ok) {
    return response;
  }
  return Promise.reject(new Error(response.statusText));
};
