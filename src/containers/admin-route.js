import React from 'react';
import * as PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Route } from 'react-router-dom';

class SomeRoute extends React.PureComponent {
  static propTypes = {
    user: PropTypes.shape({
      isAdmin: PropTypes.bool.isRequired,
      name: PropTypes.string.isRequired,
    }),
  }

  render() {
    const { user: { isAdmin }, ...other } = this.props;

    if (isAdmin) {
      return <Route {...other} />;
    }
    return null; // <Redirect to="/" />
  }
}

function mapStateToProps(state) {
  return {
    user: state.user.data,
  };
}

export const AdminRoute = connect(mapStateToProps)(SomeRoute);
