/* eslint-disable react/prop-types */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Tab, Label } from 'semantic-ui-react';
import MDSpinner from 'react-md-spinner';
import PropTypes from 'prop-types';

import { Request, ModalForDelete } from '../components';


import { getAllUserRequests, deleteRequest, updateRequest } from '../actions/request';
import { deleteUser } from '../actions/user';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  align-items: center;
  justify-content: center;
`;

const Header = styled.h3`
  text-align: center;
`;
const RequestTab = styled(Tab)`
  width: 100%;
`;
const RequestList = styled.div`
  display: flex;
  flex-direction: row;
  align-items: baseline;
  flex-wrap: wrap;
`;
const MessageLabel = styled(Label)`
  margin-top: 20px !important;
`;
const WrapperForButton = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row-reverse;
  justify-content: space-between;
`;

class Requests extends Component {
  static propTypes = {
    getAllUserRequests: PropTypes.func,
    gettingRequests: PropTypes.bool,
    errorGettingRequest: PropTypes.string,
    deleteRequest: PropTypes.func,
    updateRequest: PropTypes.func,
    deleteUser: PropTypes.func,
    errorChange: PropTypes.string,
    successChange: PropTypes.bool,
  }

  componentDidMount() {
    this.props.getAllUserRequests(this.props.match.params.redirectParam);
  }

  getTabContent(complited) {
    const {
      allUserRequests, gettingRequests, errorGettingRequest,
    } = this.props;
    const searchedRequests = allUserRequests ? allUserRequests
      .filter(item => (complited ? item.completed : !item.completed)) : [];

    if (gettingRequests) {
      return (
        <Tab.Pane>
          <MDSpinner />
        </Tab.Pane>
      );
    }
    if (errorGettingRequest) {
      return (
        <Tab.Pane>
          <Label color="red">{errorGettingRequest}</Label>
        </Tab.Pane>
      );
    }
    if (searchedRequests.length) {
      return (
        <Tab.Pane>
          <RequestList>
            {searchedRequests
              .filter(item => (complited ? item.completed : !item.completed))
              .map(item =>
                (<Request
                  data={item}
                  key={item._id}
                  onDeleteClick={id => this.props.deleteRequest(id)}
                  onChangeClick={(obj, id) => this.props.updateRequest(obj, id)}
                />))
            }
          </RequestList>
        </Tab.Pane>
      );
    }
    return (
      <Tab.Pane>
        No {complited ? <span>Closed</span> : <span>Open</span>} requests
      </Tab.Pane>
    );
  }

  render() {
    return (
      <Wrapper>
        <WrapperForButton>
          <ModalForDelete
            delFunc={() => this.props.deleteUser(this.props.match.params.redirectParam)}
            request={false}
          />
          {this.props.errorChange && <MessageLabel color="red">{this.props.errorChange}</MessageLabel>}
          {this.props.successChange && <MessageLabel color="green">Deleted</MessageLabel>}
        </WrapperForButton>
        <Header>RequestsList</Header>
        <RequestTab panes={[
          { menuItem: 'Open Requests', render: () => this.getTabContent(false) },
          { menuItem: 'Closed Requests', render: () => this.getTabContent(true) },
        ]}
        />
      </Wrapper>
    );
  }
}

function mapStateToProps(state) {
  return {
    allUserRequests: state.request.allUserRequests,
    gettingRequests: state.request.gettingRequests,
    errorGettingRequest: state.request.errorGettingRequest,
    errorChange: state.user.errorChange,
    successChange: state.user.successChange,
  };
}
export const AllUserRequests = connect(mapStateToProps, {
  getAllUserRequests, deleteRequest, updateRequest, deleteUser,
})(Requests);
