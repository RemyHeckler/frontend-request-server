import React from 'react';
import styled from 'styled-components';

import { RegisterForm, Header } from '../components';

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center
`;

export const SignUp = () => (
  <Wrapper>
    <Header />
    <RegisterForm />
  </Wrapper>
);
