import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import MDSpinner from 'react-md-spinner';
import { Link } from 'react-router-dom';
import { Button } from 'semantic-ui-react';
import PropTypes from 'prop-types';

import { ProfileCard } from '../components';

import { getAllUsers } from '../actions/user';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  padding: 0 20px;
  align-items: center;
  justify-content: center;
`;

const Header = styled.h3`
  text-align: center;
`;

const UserContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: baseline;
  flex-wrap: wrap;
`;

const CardWithButton = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const DetailsButton = styled(Button)`
  margin-top: 8px !important;
`;

class Users extends Component {
  static propTypes = {
    getAllUsers: PropTypes.func,
    allUsers: PropTypes.array,
    isLoading: PropTypes.bool,
    errorMessage: PropTypes.string,
  }

  componentDidMount() {
    this.props.getAllUsers();
  }

  getAllElements() {
    const { allUsers, isLoading, errorMessage } = this.props;
    if (isLoading) {
      return <MDSpinner />;
    }
    if (errorMessage) {
      return <div>{errorMessage}</div>;
    }
    if (allUsers) {
      return (
        <UserContainer>
          {
            allUsers.map(item => (
              <CardWithButton key={item.id}>
                <ProfileCard data={item} />
                <Link to={`/user/${item.id}`}>
                  <DetailsButton color="green">
                    Show users requests
                  </DetailsButton>
                </Link>
              </CardWithButton>))
          }
        </UserContainer>
      );
    }
    return null;
  }

  render() {
    return (
      <Wrapper>
        <Header>UsersList</Header>
        {this.getAllElements()}
      </Wrapper>
    );
  }
}

function mapStateToProps(state) {
  return {
    allUsers: state.user.allUsers,
    isLoading: state.user.isLoading,
    errorMessage: state.user.errorMessage,
  };
}

export const UsersList = connect(mapStateToProps, { getAllUsers })(Users);
