import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import { ProfileCard } from '../components';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  padding: 0 20px;
  align-items: center;
  justify-content: center;
`;

const Header = styled.h3`
  text-align: center;
`;

const ProfilePage = props => (
  <Wrapper>
    <Header>Profile</Header>
    <ProfileCard data={props.data} />
  </Wrapper>
);

ProfilePage.propTypes = {
  data: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    data: state.user.data,
  };
}

export const Profile = connect(mapStateToProps)(ProfilePage);
