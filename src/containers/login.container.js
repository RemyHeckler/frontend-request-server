import React from 'react';
import styled from 'styled-components';

import { LoginForm, Header } from '../components';

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center
`;

export const LogIn = () => (
  <Wrapper>
    <Header />
    <LoginForm />
  </Wrapper>
);
