import React from 'react';
import * as PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Route, Switch, Redirect } from 'react-router';
import { ConnectedRouter } from 'react-router-redux';

import { PageWithNavbar } from '../components';
import { Profile, Requests, RequestsList, UsersList, AdminRoute, LogIn, SignUp, AllUserRequests } from './';

export class Container extends React.PureComponent {
  static propTypes = {
    history: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
  }

  getRoutes = () => {
    const { token } = this.props.user;
    if (!token) {
      return (
        <Switch>
          <Route exact path="/" component={LogIn} />
          <Route exact path="/register" component={SignUp} />
          <Route render={() => <Redirect to="/" />} />
        </Switch>
      );
    }

    return (
      <PageWithNavbar>
        <Switch>
          <Route exact path="/" render={() => <Redirect to="/requests" />} />
          <Route exact path="/requests" component={Requests} />
          <Route exact path="/profile" component={Profile} />
          <AdminRoute exact path="/users-list" component={UsersList} />
          <AdminRoute exact path="/requests-list" component={RequestsList} />
          <AdminRoute exact path="/user/:redirectParam" component={AllUserRequests} />
          <Route render={() => <Redirect to="/" />} />
        </Switch>
      </PageWithNavbar>
    );
  }

  render() {
    return (
      <ConnectedRouter history={this.props.history}>
        {this.getRoutes()}
      </ConnectedRouter>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
    router: state.router,
  };
}

export const RootContainer = connect(mapStateToProps)(Container);
