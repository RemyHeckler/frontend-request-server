import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Tab, Label } from 'semantic-ui-react';
import MDSpinner from 'react-md-spinner';
import PropTypes from 'prop-types';

import { Request } from '../components';

import { getAllRequests, deleteRequest, updateRequest } from '../actions/request';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  padding: 0 20px;
  align-items: center;
  justify-content: center;
`;

const Header = styled.h3`
  text-align: center;
`;
const RequestTab = styled(Tab)`
  width: 100%;
`;
const RequestList = styled.div`
  display: flex;
  flex-direction: row;
  align-items: baseline;
  flex-wrap: wrap;
`;

class Requests extends Component {
  static propTypes = {
    getAllRequests: PropTypes.func,
    allRequests: PropTypes.array,
    gettingRequests: PropTypes.bool,
    errorGettingRequest: PropTypes.string,
    deleteRequest: PropTypes.func,
    updateRequest: PropTypes.func,
  }

  componentDidMount() {
    this.props.getAllRequests();
  }

  getTabContent(complited) {
    const {
      allRequests, gettingRequests, errorGettingRequest,
    } = this.props;
    const searchedRequests = allRequests ? allRequests
      .filter(item => (complited ? item.completed : !item.completed)) : [];

    if (gettingRequests) {
      return (
        <Tab.Pane>
          <MDSpinner />
        </Tab.Pane>
      );
    }
    if (errorGettingRequest) {
      return (
        <Tab.Pane>
          <Label color="red">{errorGettingRequest}</Label>
        </Tab.Pane>
      );
    }
    if (searchedRequests.length) {
      return (
        <Tab.Pane>
          <RequestList>
            {searchedRequests
              .map(item =>
                (<Request
                  data={item}
                  key={item._id}
                  onDeleteClick={id => this.props.deleteRequest(id)}
                  onChangeClick={(obj, id) => this.props.updateRequest(obj, id)}
                />))
            }
          </RequestList>
        </Tab.Pane>
      );
    }
    return (
      <Tab.Pane>
        No {complited ? <span>Closed</span> : <span>Open</span>} requests
      </Tab.Pane>
    );
  }

  render() {
    return (
      <Wrapper>
        <Header>RequestsList</Header>
        <RequestTab panes={[
          { menuItem: 'Open Requests', render: () => this.getTabContent(false) },
          { menuItem: 'Closed Requests', render: () => this.getTabContent(true) },
        ]}
        />
      </Wrapper>
    );
  }
}

function mapStateToProps(state) {
  return {
    allRequests: state.request.allRequests,
    gettingRequests: state.request.gettingRequests,
    errorGettingRequest: state.request.errorGettingRequest,
  };
}

export const RequestsList = connect(mapStateToProps, {
  getAllRequests,
  deleteRequest,
  updateRequest,
})(Requests);
