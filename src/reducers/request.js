import {
  POST_REQUEST_SUCCESS, POST_REQUEST, POST_REQUEST_FAIL, GET_REQUESTS_SUCCESS,
  GET_REQUESTS_FAIL, DELETE_REQUEST_SUCCESS, EDIT_REQUEST_SUCCESS, GET_REQUESTS,
  GET_ALL_REQUESTS_SUCCESS, GET_ALL_USER_REQUESTS_SUCCESS, LOGOUT_USER,
} from '../actions';

const initialState = {
  allRequests: null,
  requests: null,
  error: null,
  created: false,
  loading: false,
  gettingRequests: false,
  errorGettingRequest: null,
};

const request = (state = initialState, { type, payload }) => {
  switch (type) {
    case POST_REQUEST_SUCCESS: {
      return {
        ...state,
        requests: [...state.requests, payload],
        error: null,
        created: true,
        loading: false,
      };
    }
    case POST_REQUEST: {
      return {
        ...state,
        error: null,
        created: false,
        loading: true,
      };
    }
    case POST_REQUEST_FAIL: {
      return {
        ...state,
        error: payload.message,
        created: false,
        loading: false,
      };
    }
    case GET_REQUESTS_SUCCESS: {
      return {
        ...state,
        requests: payload,
        gettingRequests: false,
        errorGettingRequest: null,
      };
    }
    case GET_REQUESTS_FAIL: {
      return {
        ...state,
        requests: null,
        gettingRequests: false,
        allUserRequests: null,
        allRequests: null,
        errorGettingRequest: payload.message,
      };
    }
    case DELETE_REQUEST_SUCCESS: {
      return {
        ...state,
        requests: state.requests ? state.requests.filter(item => item._id !== payload) : null,
        allRequests: state.allRequests ? state.allRequests
          .filter(item => item._id !== payload) : null,
        allUserRequests: state.allUserRequests ? state.allUserRequests
          .filter(item => item._id !== payload) : null,
      };
    }
    case EDIT_REQUEST_SUCCESS: {
      return {
        ...state,
        requests: state.requests ? state.requests
          .map(item => (item._id !== payload._id ? item : payload)) : null,
        allRequests: state.allRequests ? state.allRequests
          .map(item => (item._id !== payload._id ? item : payload)) : null,
        allUserRequests: state.allUserRequests ? state.allUserRequests
          .map(item => (item._id !== payload._id ? item : payload)) : null,
      };
    }
    case GET_REQUESTS: {
      return {
        ...state,
        requests: null,
        allUserRequests: null,
        allRequests: null,
        gettingRequests: true,
        errorGettingRequest: null,
      };
    }
    case GET_ALL_REQUESTS_SUCCESS: {
      return {
        ...state,
        allRequests: payload,
        gettingRequests: false,
        errorGettingRequest: null,
      };
    }
    case GET_ALL_USER_REQUESTS_SUCCESS: {
      return {
        ...state,
        requests: null,
        allRequests: null,
        allUserRequests: payload,
        gettingRequests: false,
        errorGettingRequest: null,
      };
    }
    case LOGOUT_USER: {
      return initialState;
    }
    default: return state;
  }
};

export default request;
