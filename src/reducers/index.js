import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import user from './user';
import request from './request';

const reducers = combineReducers({
  router: routerReducer,
  user,
  request,
});

export default reducers;
