import { REGISTER_USER_SUCCESS, UPDATE_USER, REGISTER_USER, REGISTER_USER_FAIL,
  LOGIN_USER_SUCCESS, USER_UPDATE_FAIL, USER_UPDATE_SUCCESS, TRY_UPDATE, GET_ALL_USERS,
  FETCH_ALL_USERS, ERROR_GET_USERS, DELETE_USER_FAIL, DELETE_USER_SUCCESS,
  LOGOUT_USER,
} from '../actions';

const initialState = {
  allUsers: null,
  data: null,
  error: null,
  token: null,
  isLoading: false,
  errorMessage: null,
  errorChange: null,
  successChange: false,
  updating: false,
};

const user = (state = initialState, { type, payload }) => {
  switch (type) {
    case REGISTER_USER_SUCCESS: {
      return {
        ...state,
        data: payload,
        error: null,
        token: null,
        isLoading: false,
        errorMessage: null,
      };
    }
    case UPDATE_USER: {
      return {
        ...state,
        allUsers: state.allUsers.map(item => (item.id === payload.id ? payload : item)),
        isLoading: false,
        errorMessage: null,
      };
    }
    case REGISTER_USER: {
      return {
        ...state,
        data: null,
        error: null,
        token: null,
        isLoading: true,
        errorMessage: null,
      };
    }
    case REGISTER_USER_FAIL: {
      return {
        ...state,
        data: null,
        error: true,
        token: null,
        isLoading: false,
        errorMessage: payload.message,
      };
    }
    case LOGIN_USER_SUCCESS: {
      return {
        ...state,
        data: {
          name: payload.name,
          email: payload.email,
          isAdmin: payload.isAdmin,
        },
        error: false,
        token: payload.token,
        isLoading: false,
        errorMessage: null,
        errorChange: null,
      };
    }
    case USER_UPDATE_FAIL: {
      return {
        ...state,
        updating: false,
        errorChange: payload.message,
      };
    }
    case USER_UPDATE_SUCCESS: {
      return {
        ...state,
        updating: false,
        successChange: true,
      };
    }
    case TRY_UPDATE: {
      return {
        ...state,
        updating: true,
        errorChange: null,
        successChange: false,
      };
    }
    case GET_ALL_USERS: {
      return {
        ...state,
        allUsers: payload,
        isLoading: false,
        errorMessage: null,
      };
    }
    case FETCH_ALL_USERS: {
      return {
        ...state,
        allUsers: null,
        isLoading: true,
        errorMessage: null,
      };
    }
    case ERROR_GET_USERS: {
      return {
        ...state,
        allUsers: null,
        isLoading: false,
        errorMessage: payload.message,
      };
    }
    case DELETE_USER_FAIL: {
      return {
        ...state,
        errorChange: payload.message,
        successChange: false,
      };
    }
    case DELETE_USER_SUCCESS: {
      return {
        ...state,
        errorChange: null,
        successChange: true,
      };
    }
    case LOGOUT_USER: {
      return initialState;
    }
    default: return state;
  }
};

export default user;
