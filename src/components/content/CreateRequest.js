import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Form, Button, Label } from 'semantic-ui-react';
import MDSpinner from 'react-md-spinner';
import PropTypes from 'prop-types';
import { toast } from 'react-toastify';

import { create } from '../../actions/request';

const RequestForm = styled(Form)`
  width: 100%;
`;

class CreateBar extends Component {
  static propTypes = {
    error: PropTypes.string,
    loading: PropTypes.bool,
    create: PropTypes.func,
  }

  state = {
    title: '',
    body: '',
  }

  onSave = () => {
    this.props.create(this.state).then(() => {
      toast.success('Created');
      this.setState({ title: '', body: '' });
    });
  }

  getCallBack() {
    if (this.props.error) {
      return (
        <Label color="red">
          {this.props.error}
        </Label>
      );
    }
    if (this.props.loading) {
      return (
        <Label color="blue">
          <MDSpinner />
          Please wait...
        </Label>
      );
    }
    return null;
  }

  handleChangeInput(event, key) {
    this.setState({ [key]: event.target.value });
  }

  render() {
    return (
      <RequestForm>
        <Form.Input label="Title" type="text" value={this.state.title} onChange={e => this.handleChangeInput(e, 'title')} />
        <Form.TextArea label="Request" value={this.state.body} placeholder="Request" onChange={e => this.handleChangeInput(e, 'body')} />
        <Button
          type="submit"
          floated="right"
          disabled={!this.state.title || !this.state.body}
          onClick={this.onSave}
        >
            Create Request
        </Button>
        {this.getCallBack()}
      </RequestForm>
    );
  }
}

function mapStateToProps(state) {
  return {
    error: state.request.error,
    loading: state.request.loading,
  };
}

export const CreateRequest = connect(mapStateToProps, { create })(CreateBar);
