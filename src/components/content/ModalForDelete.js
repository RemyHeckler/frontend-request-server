import React, { Component } from 'react';
import { Button, Modal, Header } from 'semantic-ui-react';
import PropTypes from 'prop-types';

export class ModalForDelete extends Component {
  static propTypes = {
    request: PropTypes.bool,
    delFunc: PropTypes.func,
  }
  state = {
    open: false,
  }

  componentDidMount() {
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  show = () => this.setState({ open: true })
  close = () => this.setState({ open: false })

  render() {
    return (
      <div>
        {
          (this.props.request && <Button onClick={this.show} icon="close" color="red" floated="right" />) ||
          <Button color="red" onClick={this.show}>Delete User</Button>
        }
        <Modal open={this.state.open} onClose={this.close} size="small">
          <Header content="Are you sure?" />
          <Modal.Content>
            <p>Delete this {this.props.request ? <span>request</span> : <span>user</span>} ?</p>
          </Modal.Content>
          <Modal.Actions>
            <Button basic color="red" onClick={this.close}>
              No
            </Button>
            <Button
              color="green"
              onClick={() => this.props.delFunc().then(() => this._isMounted && this.close())}
            >
              Yes
            </Button>
          </Modal.Actions>
        </Modal>
      </div>
    );
  }
}
