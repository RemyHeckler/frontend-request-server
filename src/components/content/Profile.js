import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Card, Input, Button, Label, Message } from 'semantic-ui-react';
import MDSpinner from 'react-md-spinner';
import PropTypes from 'prop-types';
import { toast } from 'react-toastify';
import { isValidEmail } from '../../utils/validator';

import { change } from '../../actions/user';

const Row = styled.div`
  margin-top: 5%;
  width: 100%;
  display: flex;
  justify-content: space-between;
`;

const UserCintent = styled(Card)`
  margin-right: 25px !important;
  margin-bottom: 0px !important;
  margin-top: 20px !important;
`;

export class Profile extends Component {
  static propTypes = {
    data: PropTypes.shape({
      name: PropTypes.string,
      email: PropTypes.string,
      id: PropTypes.string,
    }),
    change: PropTypes.func,
    token: PropTypes.string,
    updating: PropTypes.bool,
  }

  state = {
    edit: false,
    name: this.props.data.name,
    email: this.props.data.email,
    password: '',
    confirmPassword: '',
  }

  getCallBack() {
    if (this.props.updating) {
      return (
        <Card.Content extra>
          <Message attached="bottom"> <MDSpinner /> Plese wait...</Message>
        </Card.Content>
      );
    }
    return null;
  }

  handleChangeInput(event, key) {
    this.setState({ [key]: event.target.value });
  }

  render() {
    const { edit } = this.state;
    if (!edit) {
      return (
        <UserCintent>
          <Card.Content>
            <Card.Header>
              Profile
            </Card.Header>
            <Row>
              <Label color="blue" ribbon>
                Name
              </Label>
              <div>
                {this.props.data.name}
              </div>
            </Row>
            <Row>
              <Label color="blue" ribbon>
                Email
              </Label>
              <div>
                {this.props.data.email}
              </div>
            </Row>
          </Card.Content>
          <Card.Content>
            <Button secondary onClick={() => this.setState({ edit: true })}>
              Edit Profile
            </Button>
          </Card.Content>
        </UserCintent>
      );
    }
    return (
      <UserCintent>
        <Card.Content>
          <Card.Header>
            Profile
          </Card.Header>
          <Row>
            <Label color="blue" ribbon>
              Name
            </Label>
          </Row>
          <Row>
            <Input
              defaultValue={this.state.name}
              onChange={e => this.handleChangeInput(e, 'name')}
            />
          </Row>
          <Row>
            <Label color="blue" ribbon>
              Email
            </Label>
          </Row>
          <Row>
            <Input
              defaultValue={this.state.email}
              onChange={e => this.handleChangeInput(e, 'email')}
            />
          </Row>
          <Row>
            <Label color="blue" ribbon>
              Password
            </Label>
          </Row>
          <Row>
            <Input
              type="password"
              onChange={e => this.handleChangeInput(e, 'password')}
            />
          </Row>
          <Row>
            <Label color="blue" ribbon>
              Confirm Password
            </Label>
          </Row>
          <Row>
            <Input
              type="password"
              onChange={e => this.handleChangeInput(e, 'confirmPassword')}
            />
          </Row>
        </Card.Content>
        <Card.Content>
          <Button
            secondary
            onClick={() => this.setState({
              edit: false,
              name: this.props.data.name,
              email: this.props.data.email,
              password: '',
              confirmPassword: '',
            })}
            floated="left"
          >
            Cancel
          </Button>
          <Button
            primary
            onClick={() => this.props.change(
            {
              id: this.props.data.id,
              name: this.state.name,
              email: this.state.email,
              password: this.state.password,
              confirmPassword: this.state.confirmPassword,
            },
            this.props.token,
            )
              .then(() => toast.success('Succes'))
              .catch(e => toast.error(e.message))
            }
            floated="right"
            disabled={
              !this.state.password ||
              !(isValidEmail(this.state.email)) ||
              !this.state.name ||
              this.state.password !== this.state.confirmPassword
            }
          >
            Save Change
          </Button>
        </Card.Content>
        {this.getCallBack()}
      </UserCintent>
    );
  }
}
function mapStateToProps(state) {
  return {
    token: state.user.token,
    errorChange: state.user.errorChange,
    successChange: state.user.successChange,
    updating: state.user.updating,
  };
}

export const ProfileCard = connect(mapStateToProps, { change })(Profile);
