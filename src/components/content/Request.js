import React, { Component } from 'react';
import styled from 'styled-components';
import { Button, Label, Card, Input } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import moment from 'moment';
import UserAvatar from 'react-user-avatar';
import { ModalForDelete } from './';

const Row = styled.div`
  margin-top: 5%;
  width: 100%;
  display: flex;
  justify-content: space-between;
`;

const CardWithMargin = styled(Card)`
  margin-right: 25px !important;
`;
const UserInfo = styled.div`
  margin-top: 12px;
  display: flex;
  justify-content: space-between;
`;
const CaseForName = styled.div`
  margin-top: 12px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: flex-end;
`;

export class Request extends Component {
  static propTypes = {
    data: PropTypes.shape({
      _id: PropTypes.string,
      title: PropTypes.string,
      body: PropTypes.string,
      completed: PropTypes.bool,
      author: PropTypes.shape({
        name: PropTypes.string,
        email: PropTypes.string,
      }),
      date: PropTypes.string,
    }),
    onChangeClick: PropTypes.func,
    onDeleteClick: PropTypes.func,
  }

  state = {
    edit: false,
    title: this.props.data.title,
    body: this.props.data.body,
  }

  handleChangeInput(event, key) {
    this.setState({ [key]: event.target.value });
  }

  render() {
    const { edit } = this.state;
    if (!edit) {
      return (
        <CardWithMargin>
          <Card.Content>
            <Card.Header>
              <ModalForDelete
                delFunc={() => this.props.onDeleteClick(this.props.data._id)}
                request
              />
            </Card.Header>
            {
              this.props.data.completed &&
                <Label color="green" tag floated="right">
                  completed
                </Label>
            }
            <Row>
              <Label color="red" ribbon>
                User
              </Label>
            </Row>
            <UserInfo>
              <UserAvatar size="50" name={this.props.data.author.name} />
              <CaseForName>
                <div>
                  {this.props.data.author.name}
                </div>
                <div>
                  {this.props.data.author.email}
                </div>
              </CaseForName>
            </UserInfo>
            <Row>
              <Label color="blue" ribbon>
                Title
              </Label>
            </Row>
            <div>
              {this.props.data.title}
            </div>
            <Row>
              <Label color="blue" ribbon>
                Body
              </Label>
            </Row>
            <div>
              {this.props.data.body}
            </div>
            <Row>
              <Label color="grey" ribbon>
                Created
              </Label>
            </Row>
            <div>
              {moment(this.props.data.date).format('MMMM Do YYYY, h:mm a')}
            </div>
          </Card.Content>
          {
            !this.props.data.completed &&
              <Card.Content extra>
                <Button primary floated="right" onClick={() => this.setState({ edit: true })}>
                Edit
                </Button>
                <Button
                  secondary
                  floated="left"
                  onClick={() => this.props.onChangeClick({
                title: this.props.data.title,
                body: this.props.data.body,
                completed: true,
              }, this.props.data._id)}
                >
                Close Request
                </Button>
              </Card.Content>
            }
        </CardWithMargin>
      );
    }
    return (
      <CardWithMargin>
        <Card.Content>
          <Row>
            <Label color="blue" ribbon>
              Title
            </Label>
          </Row>
          <div>
            <Input defaultValue={this.state.title} onChange={e => this.handleChangeInput(e, 'title')} />
          </div>
          <Row>
            <Label color="blue" ribbon>
              Request
            </Label>
          </Row>
          <div>
            <Input defaultValue={this.state.body} onChange={e => this.handleChangeInput(e, 'body')} />
          </div>
        </Card.Content>
        <Card.Content extra>
          <Button
            secondary
            floated="left"
            onClick={() => this.setState({
            edit: false,
            title: this.props.data.title,
            body: this.props.data.body,
          })}
          >
            Cancel
          </Button>
          <Button
            primary
            floated="right"
            onClick={() => this.setState({ edit: false }, () => this.props.onChangeClick({
            title: this.state.title,
            body: this.state.body,
            completed: false,
          }, this.props.data._id))}
          >
            Save
          </Button>
        </Card.Content>
      </CardWithMargin>
    );
  }
}
