import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Card, Input, Button, Label } from 'semantic-ui-react';
import styled from 'styled-components';
import MDSpinner from 'react-md-spinner';
import PropTypes from 'prop-types';

import { isValidEmail } from '../../utils/validator';

import { register } from '../../actions/user';

const DataInput = styled(Input)`
  margin-top: 10px
`;

class Form extends Component {
  static propTypes = {
    isLoading: PropTypes.bool,
    error: PropTypes.bool,
    errorMessage: PropTypes.string,
    register: PropTypes.func,
    data: PropTypes.object,
  }

  state = {
    name: '',
    email: '',
    password: '',
    confirmPassword: '',
  }

  handleChangeInput(event, key) {
    this.setState({ [key]: event.target.value });
  }

  render() {
    return (
      <Card>
        <Card.Content>
          <Card.Header>
            Sign Up
            <Link to="/">
              <Button
                secondary
                floated="right"
              >
                Log in
              </Button>
            </Link>
          </Card.Header>
          <DataInput
            error={!this.state.name}
            placeholder="Name"
            onChange={e => this.handleChangeInput(e, 'name')}
          />
          <DataInput
            error={!isValidEmail(this.state.email)}
            placeholder="Email"
            onChange={e => this.handleChangeInput(e, 'email')}
          />
          <DataInput
            error={!this.state.password}
            placeholder="Password"
            type="password"
            onChange={e => this.handleChangeInput(e, 'password')}
          />
          <DataInput
            error={
              !(this.state.password === this.state.confirmPassword) ||
              !this.state.confirmPassword
            }
            placeholder="Confirm Password"
            type="password"
            onChange={e => this.handleChangeInput(e, 'confirmPassword')}
          />
        </Card.Content>
        <Card.Content extra>
          {this.props.isLoading && <MDSpinner />}
          <Button
            primary
            floated="right"
            disabled={
              !this.state.password ||
              !(isValidEmail(this.state.email)) ||
              !this.state.name ||
              this.state.password !== this.state.confirmPassword
            }
            onClick={() => this.props.register(this.state)}
          >
            Create Account
          </Button>
        </Card.Content>
        {this.props.error &&
          <Card.Content extra>
            <Label basic color="red">
              {this.props.errorMessage}
            </Label>
          </Card.Content>
        }
        {this.props.data &&
          <Card.Content extra>
            <Label basic color="green">
              Success
            </Label>
          </Card.Content>
        }
      </Card>
    );
  }
}

function mapStateToProps(state) {
  return {
    data: state.user.data,
    isLoading: state.user.isLoading,
    error: state.user.error,
    errorMessage: state.user.errorMessage,
  };
}

export const RegisterForm = connect(mapStateToProps, { register })(Form);
