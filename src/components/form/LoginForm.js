import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Card, Input, Button, Label } from 'semantic-ui-react';
import styled from 'styled-components';
import MDSpinner from 'react-md-spinner';
import PropTypes from 'prop-types';

import { isValidEmail } from '../../utils/validator';

import { login } from '../../actions/user';

const PasswordInput = styled(Input)`
  margin-top: 10px
`;

class Form extends Component {
  static propTypes = {
    isLoading: PropTypes.bool,
    error: PropTypes.bool,
    errorMessage: PropTypes.string,
    login: PropTypes.func,
  }

  state = {
    email: '',
    password: '',
  }

  handleChangeInput(event, key) {
    this.setState({ [key]: event.target.value });
  }

  render() {
    return (
      <Card>
        <Card.Content>
          <Card.Header>
            Sign In
            <Link to="/register">
              <Button
                secondary
                floated="right"
              >
                Register
              </Button>
            </Link>
          </Card.Header>
        </Card.Content>
        <Card.Content>
          <Input
            placeholder="Email"
            error={!isValidEmail(this.state.email)}
            onChange={e => this.handleChangeInput(e, 'email')}
          />
          <PasswordInput
            placeholder="Password"
            error={!this.state.password}
            type="password"
            onChange={e => this.handleChangeInput(e, 'password')}
          />
        </Card.Content>
        <Card.Content extra>
          {this.props.isLoading && <MDSpinner />}
          <Button
            primary
            floated="right"
            disabled={!this.state.password || !(isValidEmail(this.state.email))}
            onClick={() => this.props.login(this.state)}
          >
            Login
          </Button>
        </Card.Content>
        {this.props.error &&
          <Card.Content extra>
            <Label basic color="red">
              {this.props.errorMessage}
            </Label>
          </Card.Content>
        }
      </Card>
    );
  }
}
function mapStateToProps(state) {
  return {
    token: state.user.token,
    isLoading: state.user.isLoading,
    error: state.user.error,
    errorMessage: state.user.errorMessage,
  };
}

export const LoginForm = connect(mapStateToProps, { login })(Form);
