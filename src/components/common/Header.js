import styled from 'styled-components';

export const Header = styled.div`
  background-color: #0D71BB;
  width: 100vw;
  height: 50px;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 2;
`;
