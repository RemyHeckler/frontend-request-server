import React from 'react';
import * as PropTypes from 'prop-types';
import styled from 'styled-components';
import UserAvatar from 'react-user-avatar';

const Content = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: stretch;
  width: 300px;
  height: 100%;
  @media (max-width: 800px) {
    width: 256px;
  }
`;
const Header = styled.div`
  width: 300px;
  height: 160px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  background: #00B5AD;
  padding: 20px 0;
  @media (max-width: 800px) {
    width: 256px;
  }
`;
const Links = styled.div`
  flex: 1;
  padding-top: 20px;
  background: #0D71BB;
`;
const Avatar = styled(UserAvatar)`
  div {
    border: 3px solid white;
    font-size: 24px;
    margin-top: -3px;
    line-height: 60px;
  }
`;
const Name = styled.div`
  font-size: 24px;
  font-weight: 300;
  color: white;
  text-align: center;
`;

export const SidebarContent = ({ name, children }) => (
  <Content>
    <Header>
      <Avatar size="70" name={name} />
      <Name>{name}</Name>
    </Header>
    <Links>
      {children}
    </Links>
  </Content>
);

SidebarContent.propTypes = {
  name: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)]),
};
