import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

const LinkContent = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  padding-left: 20px;
  height: 54px;
  line-height: 54px;
  font-size: 14px;
  text-transform: uppercase;
  font-weight: 500;
  cursor: pointer;
  color: #FFFFFF;
  background-color: ${({ active }) => (active ? 'rgba(0,0,0,0.2)' : null)};
  border-bottom: ${({ active }) => (active ? '1px solid rgba(0,0,0,0.2)' : '1px solid rgba(0,0,0,0)')};
  border-top: ${({ active }) => (active ? '1px solid rgba(0,0,0,0.2)' : '1px solid rgba(0,0,0,0)')};
  &:focus {
   outline: none;
  }
  &:before {
    display: ${({ active }) => (active ? 'block' : 'none')};
    position: absolute;
    left: 0;
    top: 0;
    content: '';
    width: 5px;
    height: 54px;
    background-color: #FFFFFF;
  }
`;

export const MenuLink = ({ link, active }) => {
  if (link.to) {
    return (
      <Link to={link.to}>
        <LinkContent active={active}>
          {link.title}
        </LinkContent>
      </Link>
    );
  }

  return (
    <div>
      <LinkContent tabIndex={0} role="button" onClick={link.onClick ? link.onClick : null}>
        {link.title}
      </LinkContent>
    </div>
  );
};

MenuLink.propTypes = {
  active: PropTypes.bool.isRequired,
  link: PropTypes.shape({
    title: PropTypes.string.isRequired,
    to: PropTypes.string,
    icon: PropTypes.string,
    onClick: PropTypes.func,
  }).isRequired,
};
