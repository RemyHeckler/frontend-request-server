import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import { push as Menu } from 'react-burger-menu';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Icon } from 'semantic-ui-react';
import { ToastContainer } from 'react-toastify';

import { MenuLink, SidebarContent } from './components';
import { Header } from '../';
import { logout } from '../../actions/user';

const MenuIcon = styled(Icon)`
  cursor: pointer;
  color: white;
  margin-top: 15px !important;
  margin-left: 15px !important;
  :before {
    font-size: 30px;
  }
`;
const PageContent = styled.div`
  width: calc(100vw - 350px);
  margin-top: 50px;
  @media (max-width: 800px) {
    width: 100%;
  }
`;

connect(x => ({ router: x.router, user: x.user.data }));
class Page extends Component {
  static propTypes = {
    children: PropTypes.element,
    router: PropTypes.shape({
      location: PropTypes.shape({
        pathname: PropTypes.string.isRequired,
      }).isRequired,
    }).isRequired,
    user: PropTypes.shape({
      isAdmin: PropTypes.bool.isRequired,
      name: PropTypes.string.isRequired,
    }),
    logout: PropTypes.func,
  }

  constructor(props) {
    super(props);
    this.mql = window.matchMedia('(min-width: 801px)');
    this.mql.addListener(this.handleMediaQueryChanged);
  }

  state = { open: false, docked: window.innerWidth > 800 }

  componentDidMount() {
    this.handleMediaQueryChanged();
  }

  componentWillReceiveProps(nextProps) {
    const { pathname } = this.props.router.location;
    if (pathname !== nextProps.router.location.pathname) {
      this.closeSidebar();
    }
  }

  componentWillUnmount() {
    if (this.mql) {
      this.mql.removeListener(this.handleMediaQueryChanged);
    }
  }

  getLinks = () => {
    const { isAdmin } = this.props.user;

    const links = [
      { title: 'Requests', show: true, to: '/requests' },
      { title: 'My Profile', show: true, to: '/profile' },
      { title: 'Users', show: isAdmin, to: '/users-list' },
      { title: 'All Requests', show: isAdmin, to: '/requests-list' },
      { title: 'Logout', show: true, onClick: this.props.logout },
    ].filter(link => link.show);

    return links.map(link =>
      <MenuLink key={link.title} link={link} active={this.isActive(link)} />);
  }

  handleMediaQueryChanged = () => {
    const { matches } = this.mql;
    this.setState({ open: matches, docked: matches });
  }

  isActive = (link) => {
    const { router: { location: { pathname } } } = this.props;
    const { to } = link;
    return to === pathname;
  }

  openSidebar = () => this.setState({ open: true })
  closeSidebar = () => this.setState({ open: false })

  render() {
    const { children, user: { name } } = this.props;
    const { open, docked } = this.state;

    return (
      <div id="outer-container">
        <ToastContainer autoClose={3000} />
        <Menu
          noOverlay={docked}
          disableOverlayClick={docked}
          width={docked ? 300 : 256}
          animation="reveal"
          pageWrapId="page-wrap"
          outerContainerId="outer-container"
          isOpen={open || docked}
          disableCloseOnEsc={docked}
        >
          <SidebarContent name={name}>
            {this.getLinks()}
          </SidebarContent>
        </Menu>
        <Header>
          {!docked && <MenuIcon name="bars" onClick={this.openSidebar} />}
        </Header>
        <div id="page-wrap">
          <PageContent>
            {children}
          </PageContent>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    router: state.router,
    user: state.user.data,
  };
}

export const PageWithNavbar = connect(mapStateToProps, { logout })(Page);
