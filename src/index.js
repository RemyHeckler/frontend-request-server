import 'core-js';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import createHistory from 'history/createBrowserHistory';

import 'react-toastify/dist/ReactToastify.css';

import newStore from './configureStore';

import { RootContainer } from './containers';

import registerServiceWorker from './registerServiceWorker';

import './index.css';

const { store, persistor } = newStore();

const history = createHistory();
history.listen(() => {
  const { body } = document;
  if (body && body.scrollTo) {
    body.scrollTo(0, 0);
  }
});

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <RootContainer history={history} />
    </PersistGate>
  </Provider>,
  document.getElementById('root'),
);

registerServiceWorker();
